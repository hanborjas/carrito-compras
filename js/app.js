//variables
const carrito = document.querySelector('#carrito');
const contendorCarrito = document.querySelector('#lista-carrito tbody');
const vaciarCarritoBtn = document.querySelector("#vaciar-carrito")
const listaCursos = document.querySelector('#lista-cursos');
let articulosCarrito = [];

cargarEventListeners();
function cargarEventListeners() {
    //cuando agregas un curso presionando "agregar carrito"
    listaCursos.addEventListener('click', agregarCurso);

    //elimina cursos del carrito 
    carrito.addEventListener('click', eliminarCurso);

    // vaciar el carrito
    vaciarCarritoBtn.addEventListener('click', function(){
        articulosCarrito = []; 

        limpiarHTML(); //eliminamos todo el html
    })
}

//funciones 
function agregarCurso(e){
    e.preventDefault(); 
    if(e.target.classList.contains('agregar-carrito')) {
        const cursoSeleccionado = e.target.parentElement.parentElement;
        leerDatosCursos(cursoSeleccionado);
    
    }
    
}

//eliminar un curso del carrito
function eliminarCurso(e) {
    
    if(e.target.classList.constains('.borrar-curso')) {
        const cursoId = e.target.getAttribute('data-id');

        //Elimina del array de articulosCarrito por el dataid
        articulosCarrito = articulosCarrito.filter(curso => curso.Id !== cursoId);

        
        
    }
}

//lee el contenido del html al que le das click y extrae la información del curso
function leerDatosCursos(curso){

    // crear un objecto con el contenido del curso actual 

    const infoCurso = {
        imagen: curso.querySelector('img').src,
        titulo: curso.querySelector('h4').textContent,
        precio: curso.querySelector('.precio span').textContent,
        id: curso.querySelector('a').getAttribute('data-id'),
        cantidad: 1
    }


    //revisa si un elemento ya existe en el carrito. 
    const existe = articulosCarrito.some(curso => curso.id === infoCurso.id );
    if(existe){
        //se actualiza cantidad
        const cursos = articulosCarrito.map(function(curso){
            if(curso.id === infoCurso.id ){
                curso.cantidad++;
                return curso; //retorma el objecto actualizado
            } else {
                return curso; //retorma los objetos que no son duplicados
            }
        })

        articulosCarrito.concat(cursos);
    }else{
        //agrega elementos al arreglo de carrito. 
        articulosCarrito.push(infoCurso);
    }

    

    carritoHTML(); 
}

// muestra el carrito de compras en el html 

function carritoHTML() {
    
    //limpia el html 
    limpiarHTML(); 


    articulosCarrito.forEach(function(curso){
        const row = document.createElement('tr');
        row.innerHTML = `
        <td>
            <img src="${curso.imagen}" width="100">
        </td>
        <td>${curso.titulo}</td>
        <td> ${curso.precio} </td>
        <td> ${curso.cantidad} </td>
        <td> <a href="#" class="borrar-curso" data-id="${curso.id}"> x </a> </td>
        `; 

        //agrega el HTML del carrito en el tbody 

        contendorCarrito.appendChild(row);
         
    });
}

//elimina los cursos del tbody 
function limpiarHTML(){
    // contendorCarrito.innerHTML = '';

    //limpiar el html de esta manera es mucho más performante. 
    //mientras haya un elemento hijo, va a eliminarlo. 
    // <div>
    //     <td>1</td> revisa este, se cumple la función y lo elimina
    //     <td>2</td> continúa con este, se cumple que hay hijo y lo elimina
    //     <td>3</td> continúa con este, es hijo así que lo elimina y termina, porque es el último
    // </div>
    while(contendorCarrito.firstChild){
        contendorCarrito.removeChild(contendorCarrito.firstChild)
    }
}
